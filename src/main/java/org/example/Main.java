package org.example;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;

import static org.apache.spark.sql.functions.*;
import static org.example.Database.createConnection;
import static org.example.Regime.createRegime;
import static org.example.Repas.InitializeRepas;
import static org.example.User.createUser;

public class Main {

    // Create Dataset LowGlycemiqueIndex regime
    private static Dataset<Row> createDfListLowGlycemiqueIndex(
            Dataset<Row> dataset,
            Dataset<Row> datasetRegime,
            String regime) {
        Dataset<Row> datasetFilter = dataset.filter(
                dataset.col("carbohydrates_100g").isNotNull()
                        .and(dataset.col("sugars_100g").isNotNull()));

        Dataset<Row> dataRegime = datasetRegime.filter(datasetRegime.col("regime_name").equalTo(regime));
        dataRegime.show();

        Dataset<Row> datafilterRegime = datasetFilter.join(dataRegime,
                datasetFilter.col("carbohydrates_100g").lt(dataRegime.col("regime_carbohydrates_100g"))
                        .and(datasetFilter.col("sugars_100g").lt(dataRegime.col("regime_sugars_100g"))));

        System.out.println("régime indice glycémique bas");
        System.out.println("nombre: " + datafilterRegime.count());
        datafilterRegime.show(100);
        return datafilterRegime;
    }

    // Create Dataset cetogene regime
    private static Dataset<Row> createDfListCetogene(Dataset<Row> dataset, Dataset<Row> datasetRegime, String regime) {
        int tolerance = 10;
        Dataset<Row> datasetFilter = dataset.filter(
                dataset.col("fat_100g").isNotNull()
                        .and(dataset.col("proteins_100g").isNotNull())
                        .and(expr("lower(categories) NOT LIKE '%condiment%'"))
        );
        System.out.println("dataset cétogène not null:");
        Dataset<Row> dataRegime = datasetRegime.filter(datasetRegime.col("Régime").equalTo(regime));
        dataRegime.show();

        Dataset<Row> datafilterRegime = datasetFilter.join(dataRegime,
                datasetFilter.col("fat_100g").lt(dataRegime.col("regime_fat_100g").plus(tolerance))
                        .and(datasetFilter.col("fat_100g").gt(dataRegime.col("regime_fat_100g").minus(tolerance)))
                        .and(datasetFilter.col("proteins_100g").lt(dataRegime.col("regime_proteins_100g").plus(tolerance)))
                        .and(datasetFilter.col("proteins_100g").gt(dataRegime.col("regime_proteins_100g").minus(tolerance)))
        );

        System.out.println("nombre: " + datafilterRegime.count());
        datafilterRegime.show();
        return datafilterRegime;
    }

    // Create Dataset vegetarian regime
    private static Dataset<Row> createDfListVegetarien(Dataset<Row> dataset) {
        Dataset<Row> datasetFilter = dataset.filter(
                dataset.col("ingredients_text").isNotNull()
                        .and(not(dataset.col("traces").contains("Crustacean")))
                        .and(not(dataset.col("categories").contains("Viande")))
                        .and(not(dataset.col("categories").contains("Poisson")))
        );
        System.out.println("dataset végétarien not null:");


        System.out.println("nombre: " + datasetFilter.count());
        datasetFilter.show();
        return datasetFilter;
    }

    // Create Dataset Breakfast with list of ingredients authorized by the diet
    private static void createBreakfast(Dataset<Row> listIngredients, Database database) {
        Dataset<Row> dfCereale = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Petit-déjeuners")
                                .and(listIngredients.col("categories").contains("Céréales")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(1));
        Dataset<Row> dfBoissons = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Petit-déjeuners")
                                .and(listIngredients.col("categories").contains("Boissons")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(1));
        Dataset<Row> dfFruits = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Fruits frais")
                                .or(listIngredients.col("categories").contains("Desserts lactés")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(1));

        for (int i = 0; i < 7; i++) {
            Dataset<Row> dfBreakfast = dfCereale.filter(dfCereale.col("jours").equalTo(i))
                    .union(dfBoissons.filter(dfBoissons.col("jours").equalTo(i)))
                    .union(dfFruits.filter(dfFruits.col("jours").equalTo(i)))
                    ;
            dfBreakfast.show();
            database.saveMeal(dfBreakfast);
        }

        System.out.println("céréales");
        dfCereale.show();
        System.out.println("boissons");
        dfBoissons.show();
        System.out.println("fruits");
        dfFruits.show();
    }

    // Create Dataset lunch with list of ingredients authorized by the diet
    private static void createLunch(Dataset<Row> listIngredients, Database database) {
        Dataset<Row> dfEntrees = listIngredients
                .filter(listIngredients.col("categories").contains("Entrées froides")
                        .or(listIngredients.col("categories").contains("Entrées chaudes")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(2));
        Dataset<Row> dfPlats = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Poissons")
                                .or(listIngredients.col("categories").contains("Viandes"))
                                .or(listIngredients.col("categories").contains("Plats préparés")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(2));
        Dataset<Row> dfLégumes = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Légumes"))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(2));
        Dataset<Row> dfFruits = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Fruits frais")
                                .or(listIngredients.col("categories").contains("Desserts lactés")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(2));

        for (int i = 0; i < 7; i++) {
            Dataset<Row> dfLunch = dfEntrees.filter(dfEntrees.col("jours").equalTo(i))
                    .union(dfPlats.filter(dfPlats.col("jours").equalTo(i)))
                    .union(dfLégumes.filter(dfLégumes.col("jours").equalTo(i)))
                    .union(dfFruits.filter(dfFruits.col("jours").equalTo(i)))
                    ;
            dfLunch.show();
            database.saveMeal(dfLunch);
        }

        System.out.println("entrées");
        dfEntrees.show();
        System.out.println("viandes ou poissons");
        dfPlats.show();
        System.out.println("légumes");
        dfLégumes.show();
        System.out.println("fruits");
        dfFruits.show();
    }

    // Create Dataset diner with list of ingredients authorized by the diet
    private static void createDiner(Dataset<Row> listIngredients, Database database) {
        Dataset<Row> dfEntrees = listIngredients
                .filter(listIngredients.col("categories").contains("Entrées froides")
                        .or(listIngredients.col("categories").contains("Entrées chaudes")))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(3));

        Dataset<Row> dfPlats = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Plats préparés"))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(3));

        Dataset<Row> dfDesserts = listIngredients
                .filter(
                        listIngredients.col("categories").contains("Desserts lactés"))
                .sample(0.75).limit(7)
                .withColumn("jours", functions.monotonically_increasing_id())
                .withColumn("repas", lit(3));

        for (int i = 0; i < 7; i++) {
            Dataset<Row> dfDiner = dfEntrees.filter(dfEntrees.col("jours").equalTo(i))
                    .union(dfPlats.filter(dfPlats.col("jours").equalTo(i)))
                    .union(dfDesserts.filter(dfDesserts.col("jours").equalTo(i)));
            dfDiner.show();
            database.saveMeal(dfDiner);
        }
    }

    public static void main(String[] args) {

        // Connect to database
        Database database = createConnection();

        // Create session
        SparkSession sparkSession = SparkSession
                .builder().appName("food")
                .master("local")
                .getOrCreate();
        JavaSparkContext sparkContext = JavaSparkContext.fromSparkContext(sparkSession.sparkContext());

        // Collect OpenFoodFact's Datas
        Dataset<Row> data = sparkSession
                .read()
                .format("csv")
                .option("header", "true")
                .option("delimiter", "\t")
                .load("/home/julien/Documents/epsi/Cours/Integration/livrables/openfoodfacts.csv");

        // Filtering with products sold in french Store and using columns
        Dataset<Row> dataset = data
                .filter(
                        data.col("countries").contains("France")
                                .and(data.col("serving_quantity").isNotNull()))
                .select(
                        data.col("code"),
                        data.col("product_name"),
                        data.col("brands"),
                        data.col("categories"),
                        data.col("labels"),
                        data.col("countries"),
                        data.col("ingredients_text"),
                        data.col("traces"),
                        data.col("serving_quantity").cast("integer"),
                        data.col("energy_100g").cast("float"),
                        data.col("fat_100g").cast("float"),
                        data.col("saturated-fat_100g").cast("float"),
                        data.col("proteins_100g").cast("float"),
                        data.col("carbohydrates_100g").cast("float"), //Glucides
                        data.col("sugars_100g").cast("float"), //Sucres
                        data.col("salt_100g").cast("float"),
                        data.col("fiber_100g").cast("float"),
                        data.col("glycemic-index_100g").cast("float")
                );

        // Create Régimes
        Regime regimes = createRegime(sparkContext, sparkSession);
        database.saveDataset("regimes", regimes.getDfRegime());

        // Create users dataframe
        User users = createUser(sparkContext, sparkSession);
        database.saveDataset("utilisateur", users.getDfUser());

        // Create repas
        Repas repas = InitializeRepas(sparkContext, sparkSession);
        database.saveDataset("repas", repas.getStructRepas());

        Dataset<Row> dfRegimeProductsList = createDfListLowGlycemiqueIndex(
                dataset,
                regimes.getDfRegime(),
                "Index glycémique bas");
//        Dataset<Row> dfRegimeProductsList = createDfListVegetarien(dataset);
        createBreakfast(dfRegimeProductsList, database);
        createLunch(dfRegimeProductsList, database);
        createDiner(dfRegimeProductsList, database);
    }
}