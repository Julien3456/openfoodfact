package org.example;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

public class User {
    private final Dataset<Row> dfUser;

    public User(Dataset<Row> dfUser) {
        this.dfUser = dfUser;
    }

    public static User createUser (JavaSparkContext sparkContext, SparkSession sparkSession){
        StructType structPersonne = new StructType();
        structPersonne = structPersonne.add("age", DataTypes.IntegerType);
        structPersonne = structPersonne.add("sexe", DataTypes.StringType);
        structPersonne = structPersonne.add("poids", DataTypes.IntegerType);
        structPersonne = structPersonne.add("taille", DataTypes.IntegerType);
        structPersonne = structPersonne.add("regime_id", DataTypes.IntegerType, false);

        List<Row> userList = new ArrayList<>();
        userList.add(RowFactory.create(45, "M", 75, 176, 1));
        userList.add(RowFactory.create(35, "M", 105, 170, 3));
        userList.add(RowFactory.create(25, "F", 48, 160, 1));
        userList.add(RowFactory.create(54, "F", 46, 169, 3));

        JavaRDD<Row> userRDD = sparkContext.parallelize(userList);
        Dataset<Row> dfUser = sparkSession.createDataFrame(userRDD, structPersonne);

        return new User(dfUser);
    }

    public Dataset<Row> getDfUser() {
        return dfUser;
    }
}
