package org.example;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

public class Repas {
    private final Dataset<Row> dfRepas;

    public Repas(Dataset<Row> dfRepas) { this.dfRepas = dfRepas; }


    public static Repas InitializeRepas(JavaSparkContext sparkContext, SparkSession sparkSession){
            StructType structRepas = new StructType();
            structRepas = structRepas.add("product_name", DataTypes.StringType, false);
            structRepas = structRepas.add("ingredients_text", DataTypes.StringType);
            structRepas = structRepas.add("traces", DataTypes.StringType);
            structRepas = structRepas.add("energy_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("fat_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("saturated-fat_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("proteins_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("carbohydrates_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("sugars_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("salt_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("fiber_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("glycemic-index_100g", DataTypes.IntegerType);
            structRepas = structRepas.add("jours", DataTypes.IntegerType);
            structRepas = structRepas.add("repas", DataTypes.IntegerType);
            structRepas = structRepas.add("id_regime", DataTypes.IntegerType);

        List<Row> repasList = new ArrayList<>();
        JavaRDD<Row> repasRDD = sparkContext.parallelize(repasList);
        Dataset<Row> dfRepas = sparkSession.createDataFrame(repasRDD, structRepas);

        return new Repas(dfRepas);
    }

    public Dataset<Row> getStructRepas() {
        return dfRepas;
    }
}
