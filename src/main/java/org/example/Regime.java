package org.example;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

public class Regime {
    private final Dataset<Row> dfRegime;
    public Regime(Dataset<Row> dfRegime){
        this.dfRegime = dfRegime;
    }

    public static Regime createRegime(JavaSparkContext sparkContext, SparkSession sparkSession){
        StructType structRegime = new StructType();
        structRegime = structRegime.add("regime_name", DataTypes.StringType, false);
        structRegime = structRegime.add("regime_energy_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_fat_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_proteins_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_salt_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_carbohydrates_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_sugars_100g", DataTypes.IntegerType, true);
        structRegime = structRegime.add("regime_toAvoid", DataTypes.StringType, true);

        List<Row> regimeList = new ArrayList<>();
        regimeList.add(RowFactory.create("Végétarien", null, null, null, null, null, null, "Viandes,Poissons"));
        regimeList.add(RowFactory.create("Cétogène", null, 75, 20, null, 50, null, null));
        regimeList.add(RowFactory.create("Index glycémique bas", null, null, null, null, 20, 12, null));

        Dataset<Row> dfRegime = sparkSession.createDataFrame(regimeList, structRegime)
                .withColumn("rowId", functions.monotonically_increasing_id())
                .withColumn("id_regime", functions.row_number().over(Window.orderBy("rowId")))
                .drop("rowId");;
        return new Regime(dfRegime);
    }

    public Dataset<Row> getDfRegime() {
        return dfRegime;
    }
}
