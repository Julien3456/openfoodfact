package org.example;

import org.apache.hadoop.io.MapFile;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.util.Properties;

public class Database {
    private final Properties properties;
    private final String host = "localhost";
    private final String port = "5432";
    private final String database = "foodspark";
    private final String jdbcUrl;

    public Database(Properties properties) {
        this.properties = properties;
        this.jdbcUrl = "jdbc:postgresql://" + this.host + ":" + this.port + "/" + this.database;
    }

    public Properties getProperties() {
        return properties;
    }

    public static Database createConnection(){
        String jdbcUrl = "jdbc:postgresql://localhost:5432/";
        String user = "julien";
        String password = "test";
        String jdbcDriver = "org.postgresql.Driver";

        Properties propConnection = new Properties();
        propConnection.put("user", user);
        propConnection.put("password", password);
        propConnection.put("jdbcUrl", jdbcUrl);
        propConnection.put("jdbcDriver", jdbcDriver);
        propConnection.put("dbname", "foodspark");

        return new Database(propConnection);
    }

    public void saveDataset(String table, Dataset<Row> dataset) {
        dataset.write()
                .mode(SaveMode.Overwrite)
                .jdbc(this.jdbcUrl, table, this.properties);
    }
    public void updateDataset(String table, Dataset<Row> dataset) {
        dataset.write()
                .mode(SaveMode.Append)
                .jdbc(this.jdbcUrl, table, this.properties);
    }

    public void saveMeal(Dataset<Row> dfMeal){
        Dataset<Row> dfDinerSave = dfMeal.select(
                dfMeal.col("product_name"),
                dfMeal.col("ingredients_text"),
                dfMeal.col("traces"),
                dfMeal.col("energy_100g"),
                dfMeal.col("fat_100g"),
                dfMeal.col("saturated-fat_100g"),
                dfMeal.col("proteins_100g"),
                dfMeal.col("carbohydrates_100g"),
                dfMeal.col("sugars_100g"),
                dfMeal.col("salt_100g"),
                dfMeal.col("fiber_100g"),
                dfMeal.col("glycemic-index_100g"),
                dfMeal.col("jours"),
                dfMeal.col("repas"),
                dfMeal.col("id_regime"));
        updateDataset("repas", dfDinerSave);
    }
}
